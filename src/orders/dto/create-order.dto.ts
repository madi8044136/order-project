class CreateOrderItemDto {
  productId: number;
  amount: number;
}
export class CreateOrderDto {
  customerId: number;
  orderItem: CreateOrderItemDto[];
}
