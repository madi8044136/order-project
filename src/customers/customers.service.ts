import { Injectable, NotAcceptableException } from '@nestjs/common';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from './entities/customer.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(Customer)
    private customersRepositoty: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    return this.customersRepositoty.save(createCustomerDto);
  }

  findAll() {
    return this.customersRepositoty.find({ relations: ['orders'] });
  }

  async findOne(id: number) {
    const customer = await this.customersRepositoty.findOne({
      where: { id: id },
      relations: ['orders'],
    });
    if (!customer) {
      throw new NotAcceptableException();
    }
    return customer;
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customersRepositoty.findOneBy({ id: id });
    if (!customer) {
      throw new NotAcceptableException();
    }
    const updatedCustomer = { ...customer, ...updateCustomerDto };
    return this.customersRepositoty.save(updatedCustomer);
  }

  async remove(id: number) {
    const customer = await this.customersRepositoty.findOneBy({ id: id });
    if (!customer) {
      throw new NotAcceptableException();
    }
    return this.customersRepositoty.softRemove(customer);
  }
}
